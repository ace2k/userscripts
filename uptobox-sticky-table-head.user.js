// ==UserScript==
// @name         UpToBox - Sticky Table Head
// @version      1.03
// @author       ace2k
// @license      GPL-3.0
// @description  Table head follows page scrolls for a convenient info display.
// @namespace    https://gitlab.com/ace2k
// @supportURL   https://gitlab.com/ace2k/userscripts
// @include      /^http(s|)://(www\.|)uptobox\.com/.*$/
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABgWlDQ1BzUkdCIElFQzYxOTY2LTIuMQAAKJF1kc8rRFEUxz8zgxGjUSwspJeGFRrUxMZiJobCYmaUX5uZN7/U/Hi9N5Jsle0UJTZ+LfgL2CprpYiUbGysiQ16zptRI5l7u/d87veeczr3XLBHMmrWqPFCNlfQQ0G/Mjs3rzifqKMDcKBEVUObCo9FqDreb7FZ9rrXylXd79/RGE8YKtjqhUdUTS8IjwtPrhQ0i7eEW9V0NC58ItyjS4HCN5YeK/Ozxakyf1qsR0IBsDcLK6lfHPvFalrPCsvL8WQzy+pPPdZLXIncTFhsp6x2DEIE8aMwwSgBfPQzLLuPXgbokxNV4r2l+GnyEqvKrrGKzhIp0hToEXVZsifEJkVPyMywavX/b1+N5OBAObvLD7WPpvnaBc5N+Cqa5seBaX4dymc9wHmuEp/fh6E30YsVzbMH7nU4vahosW0424C2ey2qR0uSQ5Y9mYSXY2iag5YraFgo9+znnqM7iKzJV13Czi50i7978Rspq2fKtfVxtwAAAAlwSFlzAAALEwAACxMBAJqcGAAABoxJREFUSIk9lUtsXGcZhp//P+fMjMeX2JNxfM/FuVdpQmjIhSq0VEkrUUpppSKkrAILYAFI3SKBEGukChYgsYGuEKgKICFKJKKmTQQJuZA0aRPH48Sx4wSPZzz2nDmX/8piUr7lu3q/5/0uAuBzR98SuTUiU1oU7s+O9Wye2rEu/ZgIy3uFLBTAbi8WS8NRFKKUJnB2wEkfF6S5Xyr1/r4QlZpBrO/Ums1EFgpuMnDI+qI7P3/Fi+2HvtYjjd8iA/FF690JhPyKhw1xO2Z6qsxwpUylMkAUBVy9Ps+BfRMMV/u5dG2B+wttSuVepAwQ3uO0mXVSnqs622St/kFms3Ni95Gv/9Ko7FRfORga39TH668+w+6dWzh//hY/+dErOJtjjSOKAj69W6cyVGRTtczScsb8QpN7sw3Onb/Hyy89Q6PZ4uKlOW7PrqFd6Umgkq+G1aHii9//1rGhA/s386/LNU4cn8C4iA9sG9du0qgrFh41Obh/nFY9oeQdLlQUXZld2yuMDBdI2zFvvbYd4Q3fO32U9Y7h7R//dfTKVfmzMAwUm0dCIpeSJxbXznCiBLYESY7uJKRxG7znD3++wgtHd7J1ZJqf/+IsYxMVtMqpryS42LLwsE7uiuzY2cuXjoxx9qPF/tAZh8s0pBaMBKXwgQEfILKUSo9HT1SZWWyy3lEEErAGrTQ/OH2C5uoKZ/52F6cMj5+0aSaKqeEAYS1aZYTeAdqDyvBGQ55DqMB7MIqFZfjpry7Q6OTcmXnCGyf3QBozVA5wq0v41QZee3yWgdZgA4TK8UbinCfEe1CGguowFCkinWNQ4B0AjUTwlw9rbBodRYsCCAla8eqxSYgbYCzCWYTKKQuFEh6RSjDg8ITgwBgqoePYjh429JWYW4xZXWvz8cMOdx4keKHJ83XSpM3Ff9cIsxEiAVdnmqQUmV9qsrJWZdtYPyrNKfoMnAXvCbVx1Ncy5iLJo+WYtFbi5mxC3Na88+51Hj1e4eXndjM8UMZrTb6UcT1p0dCeWzOf8vzRQzy4v8J7/yiyeThgYy9M2j5a7ZQgFITNOOX81Scsj1dJWhZUzJ7BIY58YRvv37jDY9PgnW+/wXgxAgDn8M4xlxq+eeGfTGyocPiFEW7XFvnNuVvs3TnBs3uKXLq5SBQGhAADspfD1XGmd2+gN5Q4rQjCgJ7eg/wxbjHuFWZpmf+XgB3DVQ7s2sbC/H1Ov/5lDlQHUCZjangj05Vx/tT+mIGyRPZFgsNbR9i3oUhPEqNaa+h2G7veJsxTsAbfSfDW8qCxymqS4ZSGpENJCs5dvoHKNYM64+S2MY5U+9g/OohKE0oF8x/pdQ5pB59mPF5b56PaA7Q2eK3wSuGtBa2QAs7dqVFrrSG96+6Ls8zV17j3pI7Uiu09BcaLETfu1nhYb+Qq7fxOetudIrRmvdPh4UoDpxUYDdaAtaA1wjserbZYy3KE62o4R4eQi7fu4p1DakUUBpy9ehsFH6529CcSZ/FGY1XOaCHkxc1jBFqDtSw3VsE5MKq7hM51kRkNWoGzxMZx8ZNZhBQIrYnTjGu1eVpJ+neKkQ6xXfdW5ZQ9DJSLZLkCCc1WC+EsXulusk+7cVrhAwHOIJ2nHAZ4pXBaE3lLQQiyxHitciS+60paw3Icc/HBItZoXJ4xPdSPNwa0whuFd6aLTumnmqUXzdEto7hOjFGKMMs4tGWU4f7oG2WRFSTWdRlrTStOuP24jtEKm+f0CcDobth5jtcGpxRW5V3NWHpdzpGpTZhOB6tzdNzmxPQEoTYHd0xVDsvPuBqtGC0GHB8fRn6GQXddO51j0oQDGwcYD8E+PWzeWrYO9rGtt4DTGukcTuXsHSqztb9UuFtr/FACCOEJsZSdYaIgsCoHYxBPg/VGkyUdjo8OMRlJVJ7htcJZxfPToxTShJnVNa4sN+gJJSWT8ebnd+G9f0U285yzs0vciBVyoI+h/h42lCLKxYBUgAACCaG1lJwjNIbIWQLp6eQ5L+2YRAjHvTjl7MIyi0HE2cUGlxf+ixGiJCqjk98Vxr6tUzU6WAz7941XOTQ5zFR1kPduzrKynvDuqZPs6e/p/ggAKbi2mvDar8/w2++8ydzDJc7cnufC3GOKwqfGMWPC4jURBBfF0Mi41MrIo89tmrx1p72lvtI5IJx6VnhfsYVyDe8HA5ufwtoyvjutCPAypNDfi86ULkl7naj0fmmgfyZO1e1i5GutRpb0Fgv+f9j+DVnOFEK6AAAAAElFTkSuQmCC
// @grant        none
// ==/UserScript==

(function stickyTableHead() {
    'use strict';
    var consoleCSS = 'background: #000; color: #00FF00; padding: 0px 7px; border: 1px solid #00FF00; line-height: 16px;';
    var a = document.getElementById("content-wrapper");
    var b = document.getElementsByClassName("titles")[0];
    var c = document.getElementsByClassName("checkbox-all")[0];
    var d = document.getElementsByClassName("checkbox-all")[0].getElementsByClassName("action-item")[0];
    a.style = "overflow: initial;";
    b.style = "background: #e5fbfd; color: #32abb3; box-shadow: rgba(0,0,0, 0.5) 0px 5px 3px -3px; font-weight: bold; position: sticky; top: 0; z-index: 1;";
    c.style = "top: 0;";
    d.style = "background: none;";
    console.log("%c" + GM_info.script.name + " " + GM_info.script.version,consoleCSS);
})();
