// ==UserScript==
// @name         Fandom - Reject Advertising Cookies
// @version      1.07
// @author       ace2k
// @license      GPL-3.0
// @description  Automatically reject the advertising cookies and get rid of the annoying popup.
// @namespace    https://gitlab.com/ace2k
// @supportURL   https://gitlab.com/ace2k/userscripts
// @include      *.fandom.com/*
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAA25JREFUWAntlllsDlEUx38tqqQSVNFaIqpqqa3W2mupePEg8eKFEIJYg9hbitq3ICEeLLE8eJEgtgaJfWm1aqlSW4LEFqKiQnCO+43O932d+b7hwUvvw8zcueee/2/uOffciaBd6k/+Y4v8j9q/pasAqlYg/BWIawDrVsCAvu55278PbN8ITRPc7Xyj1YiLXxrSsmEcZC+GJuK0R1d4/QaePQ+e1q83TJsEdWKgZ3fIuwWfyoLtbG9CAzRuBMsWQYNY2HsAateG4RnBEH16wfTJ8OMHvHsP9etBWg+4mQdln22S/o/uAPGNjXhsfdi2E86chcvXIKWdP0RvEZ85xUAtkTAdOizC+aDhGNAPbgjE58ohIhwrYUK8Ea9XF3btgVO5FejR0bBkHrRuBcUlkJxkxLNWmq+3LNVHtqzet++QKWBv3lojf+7OSbhgNqh4+Vd/cZ1aXg7L10DJI2ibbPqB4mr38pUIC1SM5MTs6fomqDkD6PJri65p4ml6FVcL4sFDkxfdUivG7E8tW0DNKLD82cfk2RnAbjhnhoDIsgc2O8SEsTBsiL9F3zSTmJHOMs4j6uphKZy/YGKdOd8bhCWeXwh37/uD2XruAKWPTfYfPxkexIuXoCsxa6r58oLbsGGLf2LaxPXRGUCTr2MK1KoFu/fD0ROhIXLPGfdaEwrvwNrNEn8JnZWoAeLuAFp0dBtlynbT4qP9I8fcIRJ85bewSMQ3mQTOktBpHdl3sBJ5cC5Ej5/Ah4+QMRg6tIcrUoC0tFavDhpfLUaXrsJ32ePa9N3oUVB0F1bLWRBVA7IWQovmsHUHXLxi7AKuzgBqWKoQH2DoIOjUwQdRIIGTyGndtyB6dTcxv1cMqzZADRHXQqVbUMUvXA6Qrei6A6idBTEk3QdxHfIFIiLClFr98vT+piLmrDdwGrbEliHF1X1ogECIzh1lJXwQKqwnZfEDyFkn4gKlX94qMSzx8AHsEIMHQmonSSw5Hbt18X25iGtbNFeSNClscZ0iGeWhnT5rjCeOg+bN4MlTWLEWfsqfvYont/Ykrs68AegMO4QeMnoijhwBbbyLqzvn41hH3VqG7AxdCW36ExIi241h8NX7Clg+rJUYP8aUa5etZk2p7P73K2B501L95YvV83yP9DwjcMI/iKurfwcIBPLYrwL4BU8CHfg3DJK1AAAAAElFTkSuQmCC
// @grant        none
// ==/UserScript==

var interval = setInterval(findPopup, 10);
function findPopup() {
    'use strict';
    var consoleCSS = 'background: #000; color: #00FF00; padding: 0px 7px; border: 1px solid #00FF00; line-height: 16px;';
    var cookiePopup = document.querySelectorAll('[data-tracking-opt-in-overlay="true"]');
    for (var i = 0; i < cookiePopup.length; i++) {
        cookiePopup[i].style.display = "none";
        console.log("%c" + GM_info.script.name + " " + GM_info.script.version,consoleCSS);
        if (cookiePopup[i].style.display=="none") {
            document.cookie = "Geo={%22region%22:%22AQ%22%2C%22country%22:%22AQ%22%2C%22continent%22:%22AQ%22}";
            clearInterval(interval);
            return;
        }
    }
}

// PLEASE READ CAREFULLY:
//
// Q: "document.cookie = somehaxx0ring.." - OMG YOU'RE SETTING COOKIES ON MY BEHALF! YOU'RE GONNA STEAL MY LOGINZ!!! CRIMINAL SCUM!!!
// A: YES, GIEF ME ALL YOUR LOGINZ MWAHAHAHAHAHA!!!
//    No, actually I wont. I have to modify (not set!) an existing geolocation cookie because that popup is based on said cookie.
//    All users with an EU-based IP address are affected, domestic ISP and VPN/proxy users alike. (This is directly related to EU Directive 2009/136/EC).
//    Unfortunatelly just "hiding" that popup via CSS on siteload doesn't solve the problem as it re-appears on every reload and every other page.
//    Therefore I figured out, setting the "Geo" cookie to Antarctica should fix this annoying issue... at least for now.
//    Alternatively you can just block fandom's cookies, which obviously won't allow you to sign in anymore.
//    The cookie string above is encoded and translates to - "region":"AN","country":"AN","continent":"AN" - which is Antarctica.
