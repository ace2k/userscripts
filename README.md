### Userscripts 🐵🔧
Collection of userscripts for personal use.
Get'em here or from my [GreasyFork](https://greasyfork.org/users/227) page.
