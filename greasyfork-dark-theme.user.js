// ==UserScript==
// @name         GreasyFork - Dark Theme
// @version      2.0
// @author       ace2k
// @license      GPL-3.0
// @description  Dark theme for GreasyFork.
// @namespace    https://gitlab.com/ace2k
// @supportURL   https://gitlab.com/ace2k/userscripts
// @include      /^http(s|)://(www\.|)greasyfork\.org/.*$/
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3ggEBCQHM3fXsAAAAVdJREFUOMudkz2qwkAUhc/goBaGJBgUtBCZyj0ILkpwAW7Bws4yO3AHLiCtEFD8KVREkoiFxZzX5A2KGfN4F04zMN+ce+5c4LMUgDmANYBnrnV+plBSi+FwyHq9TgA2LQpvCiEiABwMBtzv95RSfoNEHy8DYBzHrNVqVEr9BWKcqNFoxF6vx3a7zc1mYyC73a4MogBg7vs+z+czO50OW60Wt9stK5UKp9Mpj8cjq9WqDTBHnjAdxzGQZrPJw+HA31oulzbAWgLoA0CWZVBKIY5jzGYzdLtdE9DlcrFNrY98zobqOA6TJKHW2jg4nU5sNBpFDp6mhVe5rsvVasUwDHm9Xqm15u12o+/7Hy0gD8KatOd5vN/v1FozTVN6nkchxFuI6hsAAIMg4OPxMJCXdtTbR7JJCMEgCJhlGUlyPB4XfumozInrupxMJpRSRtZlKoNYl+m/6/wDuWAjtPfsQuwAAAAASUVORK5CYII=
// @grant        GM_addStyle
// @run-at       document-start
// ==/UserScript==

(function greasyforkDark() {
    'use strict';

    // Console CSS
    var consoleCSS = 'background: #000; color: #00FF00; padding: 0px 7px; border: 1px solid #00FF00; line-height: 16px;';

    // GM_addStyle fallback for Greasemonkey 4.0+
    function GM_addStyle(css) {
        const style = document.getElementById("GreasyFork_Dark_Theme") || (function() {
            const style = document.createElement('style');
            style.type = 'text/css';
            style.id = "GreasyFork_Dark_Theme";
            document.head.appendChild(style);
            return style;
        })();
        const sheet = style.sheet;
        sheet.insertRule(css, (sheet.rules || sheet.cssRules || []).length);
    }

    // Color scheme definition
    GM_addStyle(`:root {
                 --grey-bright: #232323;
                 --grey-dark: #1b1b1b;
                 --green-bright: #499900;
                 --green-dark: #0c6700;
    }`);

    // General
    GM_addStyle('body { background-color: var(--grey-dark); color: #d8d8d8; }');
    GM_addStyle('a { color: var(--green-bright); text-decoration: none; }');
    GM_addStyle('a:visited { color: var(--green-bright); }');
    GM_addStyle('a:hover { color: #00ff00; }');
    GM_addStyle('button, textarea, select, input[type=email], input[type=password], input[type=text], input[type=submit], input[type=search], input[type=url], input[type=number], input[name=add-automatic-script-set-value-2], input[name=add-automatic-script-set-value-3] { height: 28px; outline: none; color: #fff; background: #3a3a3a; border: 1px solid var(--green-dark); border-radius: 3px; padding: 3px 5px }');
    GM_addStyle('button { cursor: pointer; outline: none; }');
    GM_addStyle('textarea, #add-automatic-script-set-value-4 { min-height: 10em; }');
    GM_addStyle('input[type=file], input.add-screenshot-captions { background: var(--grey-dark); border: 1px solid var(--green-bright); }');
    GM_addStyle('input[type=search]:focus { border: 1px solid var(--green-bright); }');
    GM_addStyle('input[type=submit]:hover, input[type=file]:hover { cursor: pointer; background: var(--green-bright); color: #fff; }');
    GM_addStyle('code { padding: 1px; background: #3a3a3a; border-color: #3a3a3a; border-radius: 5px; }');
    GM_addStyle('.notice { background: #3a3a3a; color: #fff; font-weight: bold; border: 1px solid #00ff00; border-left: 6px solid #00ff00; }');
    GM_addStyle('.alert { background: #3a3a3a; color: #fff; font-weight: bold; border: 1px solid #ffdd00; border-left: 6px solid #ffdd00; }');

    // Header
    GM_addStyle('#main-header { background: linear-gradient(var(--green-dark), var(--green-bright)); }');
    GM_addStyle('#site-name { height: 48px; }');
    GM_addStyle('#site-name img { height: 42px; }');
    GM_addStyle('#site-name-text h1 { font-size: 42px; }');
    GM_addStyle('.with-submenu nav { background-color: var(--green-bright); }');
    GM_addStyle('.with-submenu nav li:hover { background-color: var(--green-dark); }');

    // Body & sidebar
    GM_addStyle('#user-profile, #additional-info>div { background: var(--grey-dark); border: 1px solid #313131; box-shadow: inset 0 0 5px 0 #0e0e0e; }');
    GM_addStyle('#browse-user-list, #script-info, .text-content, .discussion-list, .script-list, .list-option-groups ul { background-color: var(--grey-bright); border: 3px double var(--grey-dark); box-shadow: 0 0 5px 0 rgba(14, 14, 14, 0.5); }');
    GM_addStyle('.text-content #home-script-nav { max-width: 100%; border-bottom: 1px solid var(--grey-dark); }');
    GM_addStyle('.script-list h2 { font-size: 16px; }');
    GM_addStyle('.script-list li, .script-list li:not(.ad-entry) { border-bottom: 1px solid var(--grey-dark); }');
    GM_addStyle('.list-option-group .list-current { border-left: 7px solid var(--green-bright); background: linear-gradient(#2b2b2b, var(--grey-dark)); }');
    GM_addStyle('.list-option-group a:hover, .list-option-group a:focus { background: linear-gradient(var(--green-bright), var(--green-dark)); box-shadow: none; color: #fff; }');
    GM_addStyle('.sidebar { margin-inline-start: 15px; }');
    GM_addStyle('.list-option-button { background: var(--grey-bright); border: none; border-radius: 3px; box-shadow: rgba(14, 14, 14, 0.5) 0px 0px 5px 0px; color: var(--green-bright)!important; }');
    GM_addStyle('.list-option-button:hover { background: var(--green-bright); color: #fff!important; }');
    GM_addStyle('.pagination { -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; }');
    GM_addStyle('.pagination > *, .script-list+.pagination > *, .user-list+.pagination > * { background: var(--grey-bright); border-radius: 3px; box-shadow: rgba(14, 14, 14, 0.5) 0px 0px 5px 0px; margin-bottom: 14px; font-weight: 600; padding: 0.25em 0.5em; }');
    GM_addStyle('.pagination .current { background-color: var(--green-dark); }');
    GM_addStyle('.pagination > a:hover, .pagination>a:focus { background-color: var(--green-bright); color: #fff; }');
    GM_addStyle('.expander { color: #fff; background: var(--green-bright); text-decoration: none; }');
    GM_addStyle('.expander:hover { background: var(--green-dark); }');

    // Discussion
    GM_addStyle('.discussion-list-main-content { padding: 0 5px; }');
    GM_addStyle('.discussion-header, .discussion-list-header { margin-top: 36px; margin-bottom: 15px; }');
    GM_addStyle('.discussion-list { margin: 0 0 14px 0; }');
    GM_addStyle('.discussion-list-logged-in .discussion-read { background-color: unset; }');
    GM_addStyle('.discussion-list-item { border-top: 1px solid var(--grey-dark); }');
    GM_addStyle('.rating-icon { border-radius: 3px; font-size: 10px; font-weight: 600; padding: 2px 0; text-shadow: 1px 1px 0 #000; }');
    GM_addStyle('.rating-icon-good { background: rgba(77, 166, 77, 0.1); }');
    GM_addStyle('.rating-icon-ok { background: rgba(211, 211, 77, 0.1); }');
    GM_addStyle('.rating-icon-bad { background: rgba(255, 77, 77, 0.1); }');
    GM_addStyle('.badge { border-radius: 3px; padding: 2px 3px; }');

    // Authentication page
    GM_addStyle('form.new_user, form.external-login-form { background: var(--grey-dark); border: 1px solid #313131; box-shadow: inset 0 0 5px 0 #0e0e0e; }');
    GM_addStyle('form.new_user input[type="submit"] { background: linear-gradient(var(--green-dark), var(--green-bright)); }');
    GM_addStyle('form.new_user input[type="submit"]:hover {cursor: pointer; background: linear-gradient(#108800, #62ce00); }');
    GM_addStyle('form.new_user input[type="text"], form.new_user input[type="email"], form.new_user input[type="password"] { color: #fff; background: #3a3a3a; border: 1px solid #313131; }');
    GM_addStyle('button.g-recaptcha { display: block; width: 100%; min-height: 34px; box-sizing: border-box; margin: 0.5em 0 0 0; padding: 6px 8px; font-size: 14px; font-weight: bold; line-height: 20px; text-align: center; vertical-align: middle; color: #fff; background: linear-gradient(var(--green-dark), var(--green-bright)); border: 0px solid #ddd; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19); border-radius: 3px; white-space: normal; }');
    GM_addStyle('button.g-recaptcha:hover { background: linear-gradient(#108800, #62ce00); }');
    GM_addStyle('.external-login-container button { padding: 10px 10px 10px 35px; background-position: 10px; line-height: 0px; }');

    // Profile page
    GM_addStyle('a.discussion-title { color: var(--green-bright); }');
    GM_addStyle('a.discussion-title:hover { color: #00ff00; }');
    GM_addStyle('#user-control-panel, #user-script-sets { list-style: none; padding-inline-start: 0px; }');
    GM_addStyle('#user-control-panel li::before, #user-script-sets li::before { content: "\\00BB\\20"; }');

    // Scripts page
    GM_addStyle('.tabs .current { background: var(--grey-dark); border-top: 7px solid var(--green-bright); }');
    GM_addStyle('.user-content { background: unset; border-left: unset; padding: unset; }');
    GM_addStyle('#user-profile, .user-screenshots { padding: 0.5em }');
    GM_addStyle('.prettyprint { background: #e2e2e2 }');
    GM_addStyle('.history_versions { color: var(--grey-bright); list-style: none; padding-inline-start: 0; }');
    GM_addStyle('.history_versions li { color: #d8d8d8 }');
    GM_addStyle('.diff { color: #d8d8d8; }');
    GM_addStyle('.diff ul { background: #3f3f3f; }');
    GM_addStyle('.diff li:hover { background: #242424; }');
    GM_addStyle('.diff li.diff-block-info { color: #fff; }');
    GM_addStyle('.diff li.diff-block-info:hover { background: gray; }');
    GM_addStyle('.diff ins, .diff .ins, .diff .ins strong { background: #152515; }');
    GM_addStyle('.diff del, .diff .del, .diff .del strong { background: #2b1b1b; }');
    GM_addStyle('.form-control button { margin-top: 10px; }');
    GM_addStyle('#new_script_set section { border-bottom: 3px double #1b1b1b; }');
    GM_addStyle('#version-note { background-color: var(--grey-dark); border: 2px dotted var(--green-bright); }');

    // Console feedback
    console.log("%c" + GM_info.script.name + " " + GM_info.script.version,consoleCSS);
})();
